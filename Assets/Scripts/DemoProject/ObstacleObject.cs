using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DemoProject
{
    public class ObstacleObject : MonoBehaviour, IPointerClickHandler
    {
        public PathFinder PathFinder;
        private GridPosition _gridPosition;
        private UserActionController _controller;

        private void Start()
        {
            if (!PathFinder)
            {
                Destroy(gameObject);
            }
            else
            {
                _gridPosition = new GridPosition(transform.position);

                //Register obstacle in PathFinder 
                if (PathFinder.RegisterObstacle(_gridPosition))
                {
                    //Center object on grid
                    transform.position = _gridPosition.ToVector() + transform.localScale / 2;
                }
                else
                {
                    //Destroy self if there is another obstacle
                    Destroy(gameObject);
                }
            }
        }

        public void Annihilate()
        {
            if (PathFinder)
            {
                PathFinder.UnregisterObstacle(_gridPosition);
            }

            Destroy(gameObject);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (PathFinder)
            {
                if (_controller == null)
                {
                    _controller = PathFinder.GetComponent<UserActionController>();
                }

                if (_controller != null && _controller.currentUserActionMode ==
                    UserActionController.UserActionMode.PLACE_OBSTACLES)
                {
                    Annihilate();
                }
            }
        }
    }
}