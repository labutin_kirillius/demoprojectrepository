using System;
using UnityEngine;
using UnityEngine.UI;
using static DemoProject.UserActionController.UserActionMode;

namespace DemoProject
{
    public class ModeButton : MonoBehaviour
    {
        public Text TextField;
        public UserActionController Controller;


        public void ToggleMode()
        {
            if (!Controller) return;

            switch (Controller.currentUserActionMode)
            {
                case PLACE_OBSTACLES:
                    Controller.currentUserActionMode = PATH_SELECTION;
                    break;
                case PATH_SELECTION:
                    Controller.currentUserActionMode = PLACE_OBSTACLES;
                    break;
            }

            SetDescription();
        }

        private void Awake()
        {
            SetDescription();
        }

        private void SetDescription()
        {
            if (!TextField || !Controller)
            {
                return;
            }
            switch (Controller.currentUserActionMode)
            {
                case PLACE_OBSTACLES:
                    TextField.text = "Add obstacles";
                    break;
                case PATH_SELECTION:
                    TextField.text = "Select path";
                    break;
            }
        }
    }
}