using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DemoProject
{
    [RequireComponent(typeof(PathFinder))]
    public class UserActionController : MonoBehaviour, IPointerClickHandler
    {
        public Camera Camera;
        public UserActionMode currentUserActionMode;
        public ObstacleObject ObstaclePrefab;


        private PathFinder _pathFinder;
        private GridPosition _firstPosition;
        private LineRenderer _renderer;
        private bool _isFirst = true;


        public enum UserActionMode
        {
            PLACE_OBSTACLES,
            PATH_SELECTION
        }

        private void Awake()
        {
            _pathFinder = GetComponent<PathFinder>();
            _renderer = GetComponent<LineRenderer>();
        }

        private ObstacleObject FindObstacle(Vector3 position)
        {
            var mask = LayerMask.GetMask("Obstacle");
            var hits = Physics.RaycastAll(position + Vector3.up * 3, Vector3.down, mask);
            if (hits.Length > 0)
            {
                return hits[0].transform.GetComponent<ObstacleObject>();
            }
            else
            {
                return null;
            }
        }

        private void PlaceObstacle(Vector3 position)
        {
            //create new obstacle
            var obstacle = Instantiate(ObstaclePrefab);
            obstacle.transform.position = position;

            if (obstacle)
            {
                obstacle.PathFinder = _pathFinder;
            }
            else
            {
                Destroy(obstacle);
            }
        }

        private void PerformPathSelection(Vector3 position)
        {
            if (_isFirst)
            {
                _firstPosition = new GridPosition(position);
            }
            else
            {
                var path = _pathFinder.FindPath(_firstPosition,
                    new GridPosition(position));
                if (path != null)
                {
                    _renderer.positionCount = path.Count;
                    _renderer.SetPositions(path.ToArray());
                }
                else
                {
                    _renderer.positionCount = 0;
                }
            }

            _isFirst = !_isFirst;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!Camera || !ObstaclePrefab || !_renderer)
            {
                return;
            }

            var hits = Physics.RaycastAll(Camera.ScreenPointToRay(Input.mousePosition));
            if (hits.Length == 0)
            {
                return;
            }

            var clickPositionOnScene = hits[0].point;
            if (hits[0].transform.GetComponent<ObstacleObject>() != null) Debug.Log("OBSTACLE!!!!");

            switch (currentUserActionMode)
            {
                case UserActionMode.PLACE_OBSTACLES:
                    PlaceObstacle(clickPositionOnScene);
                    break;
                case UserActionMode.PATH_SELECTION:
                    PerformPathSelection(clickPositionOnScene);
                    break;
            }
        }
    }
}