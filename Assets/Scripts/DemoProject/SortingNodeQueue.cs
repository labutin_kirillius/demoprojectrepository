﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DemoProject
{
    internal class SortingNodeQueue : List<Node>
    {
        public void Enqueue(Node node)
        {
            Add(node);
        }

        public Node Dequeue()
        {
            if (Count == 0) throw new IndexOutOfRangeException();
            var node = this[Count - 1];
            RemoveAt(Count - 1);
            return node;
        }

        public void SortByCostToTarget(GridPosition target)
        {
            Sort(delegate(Node a, Node b)
            {
                var costA = a.Cost + a.Position.GetDistanceTo(target);
                var costB = b.Cost + b.Position.GetDistanceTo(target);

                var delta = costA - costB;

                return Mathf.Abs(delta) < 0.1f ? 0 : (delta < 0 ? 1 : -1);
            });
        }
    }
}