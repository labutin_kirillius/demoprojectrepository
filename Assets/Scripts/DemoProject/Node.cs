﻿namespace DemoProject
{
    internal class Node
    {
        public Node(float cost, Node parent, GridPosition position)
        {
            Cost = cost;
            Parent = parent;
            Position = position;
        }

        public float Cost { get; set; }

        public Node Parent { get; set; }

        public GridPosition Position { get; }

        protected bool Equals(Node other)
        {
            return Position.Equals(other.Position);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Node) obj);
        }

        public override int GetHashCode()
        {
            return Position.GetHashCode();
        }
    }
}