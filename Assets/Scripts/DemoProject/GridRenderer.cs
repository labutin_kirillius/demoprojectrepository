using System;
using UnityEngine;

namespace DemoProject
{
    public class GridRenderer : MonoBehaviour
    {
        public Material LineMaterial;
        public Color LineColor = Color.red;
        public Vector2 GridSize = new Vector2(50, 50);
        public Vector3 GridCenter = Vector3.zero;

        private void OnPostRender()
        {
            //Draw grid in Play mode
            DrawGrid();
        }
        private void OnDrawGizmos()
        {
            //Draw grid in Unity Editor mode
            DrawGrid();
        }

        private void DrawGrid()
        {
            var offset = GridCenter - new Vector3(GridSize.x / 2, 0, GridSize.y / 2);
            for (int x = 0; x <= GridSize.x; x++)
            {
                DrawLine(offset + new Vector3(x, 0, 0),
                    offset + new Vector3(x, 0, GridSize.y), LineColor);
            }

            for (int y = 0; y <= GridSize.y; y++)
            {
                DrawLine(offset + new Vector3(0, 0, y),
                    offset + new Vector3(GridSize.x, 0, y), LineColor);
            }
        }

        private void DrawLine(Vector3 from, Vector3 to, Color color)
        {
            if (!LineMaterial) return;
            GL.Begin(GL.LINES);
            LineMaterial.SetPass(0); 
            GL.Color(color);
            GL.Vertex3(from.x, from.y, from.z);
            GL.Vertex3(to.x, to.y, to.z);
            GL.End();
        }
    }
}