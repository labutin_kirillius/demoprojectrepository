﻿using System;
using UnityEngine;

namespace DemoProject
{
    [Serializable]
    public struct GridPosition
    {
        public static Vector3 CellOffset = new Vector3(0.5f, 0, 0.5f);

        public override string ToString()
        {
            return $"{nameof(x)}: {x}, {nameof(y)}: {y}";
        }

        public bool Equals(GridPosition other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            return obj is GridPosition other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (x * 397) ^ y;
            }
        }

        public int x;
        public int y;

        public float GetDistanceTo(GridPosition position)
        {
            return (ToVector() - position.ToVector()).magnitude;
        }

        public GridPosition(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public GridPosition(Vector3 source)
        {
            x = (int) Mathf.Floor(source.x);
            y = (int) Mathf.Floor(source.z);
        }

        public Vector3 ToVector()
        {
            return new Vector3(x, 0, y);
        }
    }
}