﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DemoProject
{
    [RequireComponent(typeof(LineRenderer))]
    public class PathFinder : MonoBehaviour
    {
        
        public Vector2 Bounds;
        private List<GridPosition> _obstacles = new List<GridPosition>();

        public List<Vector3> FindPath(GridPosition from, GridPosition to)
        {
            if (!CheckPositionInBounds(from) || !CheckPositionInBounds(to) || _obstacles.Contains(from) ||
                _obstacles.Contains(to)) return null;
            var path = new List<Vector3>();
            var startNode = new Node(0, null, from);

            var passedNodes = new List<Node>();
            var pendingNodes =
                new SortingNodeQueue();


            passedNodes.Add(startNode);

            //Add current node neighbors to pending queue

            var neighbors = GetNodeNeighbors(startNode);

            for (int i = 0; i < neighbors.Count; i++)
            {
                pendingNodes.Enqueue(neighbors[i]);
            }


            while (pendingNodes.Count > 0)
            {
                pendingNodes.SortByCostToTarget(to);
                var node = pendingNodes.Dequeue();
                //Check that current node is destination
                if (node.Position.Equals(to))
                {
                    do
                    {
                        path.Add(node.Position.ToVector() + GridPosition.CellOffset);
                        node = node.Parent;
                    } while (node != null);

                    return path;
                }


                //Check node is passed
                if (passedNodes.Contains(node)) continue;

                //Add node to passed list
                passedNodes.Add(node);

                //Check all node neighbors
                neighbors = GetNodeNeighbors(node);

                foreach (var neighbor in neighbors)
                {
                    //Check pending contains neighbor
                    if (!pendingNodes.Contains(neighbor))
                    {
                        pendingNodes.Enqueue(neighbor);
                    }
                }
            }


            return null;
        }


        private bool CheckPositionInBounds(GridPosition position)
        {
            return position.x >= -Bounds.x / 2 && position.x < Bounds.x / 2 && position.y >= -Bounds.y / 2 &&
                   position.y < Bounds.y / 2;
        }

        private List<Node> GetNodeNeighbors(Node node)
        {
            var position = node.Position;
            var result = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (y != 0 || x != 0)
                    {
                        var newPosition = new GridPosition(position.x + x, position.y + y);

                        if (CheckPositionInBounds(newPosition) && !_obstacles.Contains(newPosition))
                        {
                            result.Add(new Node(position.GetDistanceTo(newPosition) + node.Cost, node, newPosition));
                        }
                    }
                }
            }


            return result;
        }

        public bool RegisterObstacle(GridPosition position)
        {
            if (!_obstacles.Contains(position))
            {
                _obstacles.Add(position);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UnregisterObstacle(GridPosition position)
        {
            _obstacles.Remove(position);
        }
    }
}